#ifndef FILE_H
#define FILE_H

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

typedef struct s_file_node
{
  int* array;
  size_t array_size;
  
  size_t index;
  struct s_file_node* next;
  
} file_node_t;

typedef struct
{
  file_node_t* first;
  file_node_t* last;
  
} file_t;

void file_create(file_t* self);
void file_destroy(file_t *self);
void file_add(file_t* self, int* array, size_t array_size);
int file_contains(file_t* self, int* array, size_t array_size);
file_node_t* file_find(file_t* self, int* array, size_t array_size);
void file_dump(file_t* self);

void file_node_init(file_node_t* self);
int file_node_equals(file_node_t* self, int* array, size_t size);
void file_node_set(file_node_t* self, int* array, size_t size);
void file_node_dump(file_node_t* self);
void file_node_destroy(file_node_t *self);


#endif // FILE_H
