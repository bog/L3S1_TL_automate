#ifndef AUTOMATE_H
#define AUTOMATE_H
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <liste.h>
#include <graph.h>
#include <file.h>

#define AUTOMATE_VALID 1
#define AUTOMATE_INVALID 0

typedef struct
{
  int size;
  int sizealpha;
  int* initial;
  int* final;
  liste_t*** trans;

  int status;
  
} automate_t;

void automate_create(automate_t* automate, size_t size, size_t sizealpha);
void automate_create_produit(automate_t* self, automate_t* src1, automate_t* src2);
void automate_destroy(automate_t* automate);
void automate_status(automate_t *automate);

void construitAutomateExemple(automate_t* automate);

int is_the_last_of(automate_t* automate, int* special_state, size_t state);

void ajouteTransition(automate_t* automate, size_t beg, size_t end, char label);
void supprimeTransition(automate_t* automate, size_t beg, size_t end, char label);

void ajouteEtat(automate_t* automate);
void supprimeEtat(automate_t* automate, size_t state_to_remove);

void afficheAutomate(automate_t* automate);
int compteTransition(automate_t* automate);

int deterministe(automate_t* automate);

int complet(automate_t* automate);
void completeAutomate(automate_t* automate);
void fusionEtat(automate_t* automate, size_t state_1, size_t state_2);

void automateToGraph(automate_t* automate, graph_t* graph);
void automate_graph_print(automate_t* self);
int langageVide(automate_t* automate);

int est_accessible(automate_t *automate, size_t state);
void supprime_non_accessible(automate_t *automate);

int est_co_accessible(automate_t *automate, size_t state);
void supprime_non_co_accessible(automate_t *automate);


void automate_create_deterministe(automate_t* automate, automate_t* model);
// return the number of reached states from from_state
// array must size must be equals to automate->size
// array[i] == 1 if the state i is reached, 0 else.
size_t automate_transitions_from(automate_t* automate, size_t from_state,
				       char from_alpha, int* array);
void automate_create_deterministe_init(automate_t* automate, automate_t* model, file_t* file);
void automate_create_deterministe_transition_from_letter(automate_t* automate, automate_t* model, file_t* file, file_node_t* current, char letter);

void automate_complement(automate_t* automate);
int automate_include(automate_t* automate, automate_t* other);

int automate_nerode_equivalent(automate_t* automate, size_t s1, size_t s2);
int automate_is_minimal(automate_t* automate);
void automate_minimise_nerode(automate_t* automate);

void automate_build_from_state(automate_t* automate, automate_t* other, size_t from_state);

void automate_minimise_hopcroft(automate_t* automate);

void automate_minimise_hopcroft_fusion(automate_t* automate, file_t* file);

#endif//AUTOMATE_H
