#ifndef LISTE_H
#define LISTE_H

typedef struct liste_t
{  
  int state;
  struct liste_t* suiv;
  
} liste_t;

void ajouteListe(liste_t** l,int q);
int compteListe(liste_t* li);

void creerListe(liste_t* liste);
void detruitListe(liste_t* liste);

void afficheListe(liste_t* liste);
int estVideListe(liste_t* liste);


#endif // LISTE_H
