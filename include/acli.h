// Automata Command Line Interface
#ifndef ACLI_H
#define ACLI_H
#include <stdio.h>
#include <automate.h>
#define N_MAX_AUTOMATE 10

typedef struct
{
  automate_t* automates[N_MAX_AUTOMATE];
  size_t current_automate;
} acli_t;

void acli_init(acli_t* self);
void acli_destroy(acli_t* self);

void print_title();
void print_menu(acli_t* acli);
void get_user_char(char* str, size_t size);
void get_user_transition(automate_t* automate, size_t* start, size_t* end, char* label);
int get_user_state(automate_t* automate, size_t* state);
int execute_function(acli_t* acli, char* name);

void switch_automate(acli_t* acli);
void build_new(automate_t* automate);
void build_example(acli_t* acli);
void add_transition(automate_t* automate);
void delete_transition(automate_t* automate);
void add_state(automate_t* automate);
void delete_state(automate_t* automate);
void complete(automate_t* automate);
void print_info(automate_t* automate);
void define_initial(automate_t* automate);
void define_final(automate_t* automate);
void delete_none_accessible(automate_t* automate);
void delete_none_co_accessible(automate_t* automate);
void fusion_state(automate_t* automate);
void compute_product(acli_t* acli);
void compute_deterministe(acli_t* acli);
void delete_current(acli_t *acli);
void compute_complement(acli_t* acli);
void compute_minimise_nerode(acli_t* acli);
void compute_minimise_hopcroft(acli_t* acli);
#endif // ACLI_H
