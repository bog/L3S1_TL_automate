#ifndef GRAPH_H
#define GRAPH_H
#include <stdio.h>
#include <stdlib.h>
#include <liste.h>

typedef struct
{
  liste_t** data; // ** = array of lists
  size_t capacity; // TODO better comment
  size_t count; // idem
  size_t size;
  
} graph_t;

void graph_create(graph_t* self);
void graph_destroy(graph_t* self);
void graph_dump(graph_t* self);

void graph_add(graph_t* self, size_t data, int val);
void graph_increase(graph_t* self);
void graph_fit_to_size(graph_t* self, size_t size);

int graph_path(graph_t* graph, size_t from, size_t to);
void graph_mark_from(graph_t* graph, size_t from);

#endif // GRAPH_H


