#include <graph.h>
#include <assert.h>

void graph_create(graph_t* self)
{
  assert(self);
  self->capacity = 1;
  self->count = 0;
  self->size = 0;
  self->data = calloc(self->capacity, sizeof(liste_t*));
  
  self->data[0] = malloc(sizeof(liste_t));
  creerListe(self->data[0]);
}

void graph_destroy(graph_t* self)
{
  assert(self);

  for(size_t i=0; i<self->capacity; i++)
    {
      detruitListe(self->data[i]);
    }

  free(self->data);

  
  self = NULL;
}

void graph_dump(graph_t* self)
{
  assert(self);

  for(size_t data=0; data<self->size; data++)
    {
      printf("%zd| ", data);

      if( self->data[data] != NULL
	  && estVideListe(self->data[data]) == 0 )
	{
	  afficheListe(self->data[data]);
	}

      printf("\n");
      
    }
}

void graph_add(graph_t* self, size_t data, int val)
{
  assert(self);

  while(data >= self->capacity)
    {
      graph_increase(self);
    }

  ajouteListe(&self->data[data], val);
  self->count++;

  if( data+1 > self->size ){ self->size = data+1; }
  if( val+1 > self->size ){ self->size = val+1; }
}

void graph_increase(graph_t* self)
{
  assert(self);

  size_t const tmp_size = self->capacity;
  liste_t* tmp[tmp_size];
  
  for(size_t i=0; i<tmp_size; i++)
    {
      tmp[i] = self->data[i];
    }

  free(self->data);
  
  self->data = calloc(self->capacity*2, sizeof(liste_t*));
  
  // create lists
  for(size_t i=0; i<self->capacity; i++)
    {
      self->data[i] = tmp[i];
    }

  for(size_t i=self->capacity; i<self->capacity*2; i++)
    {
      self->data[i] = malloc(sizeof(liste_t));
      creerListe(self->data[i]);
    }
  
  self->capacity *= 2;
      
}

void graph_fit_to_size(graph_t* self, size_t size)
{
  self->size = size;
  while( self->capacity < self->size )
    {
      graph_increase(self);
    }
}

void graph_path_partial(graph_t* graph, size_t from, int* marked_nodes)
{
  assert(graph);
  assert(marked_nodes);

  liste_t* itr = graph->data[from]->suiv;
  
  marked_nodes[from] = 1;
  
  while( itr != NULL )
    {

      if(marked_nodes[itr->state] == 0)
	{
	  graph_path_partial(graph, itr->state, marked_nodes);
	}

      itr = itr->suiv;
    }

}

int graph_path(graph_t* graph, size_t from, size_t to)
{
  assert(graph);
  assert(graph->size > 0);
  assert(from < graph->size);
  assert(to < graph->size);

  int* marked_nodes = calloc(graph->size, sizeof(int));

  graph_path_partial(graph, from, marked_nodes);

  int path_exists = (marked_nodes[to] == 1);
  
  free(marked_nodes);
  
  return path_exists;
}
