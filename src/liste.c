#include <liste.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

void ajouteListe(liste_t** l,int q)
{
  liste_t* ptl;
  liste_t* tmp;
  
  ptl = *l;

  // we have to create the first element of the list
  if(!ptl)
    {
      ptl=(liste_t*) malloc(sizeof(liste_t));
      ptl->state = q;
      ptl->suiv = NULL;
      *l = ptl;
      return;
    }

  // the first element has the good value, we don't add q
  if(ptl->state == q)
    {
      return;
    }

  // we add it just before the first element
  if(q < ptl->state)
    {
      tmp = *l;
      *l = (liste_t*) malloc(sizeof(liste_t));
      (*l)->state=q;
      (*l)->suiv=tmp;
      return;
    }

  // ptl become the place
  while(ptl->suiv && ptl->suiv->state < q)
    {
      ptl = ptl->suiv;
    }

  // we reach the end of the list
  if(!ptl->suiv)
    {
      ptl->suiv=(liste_t*) malloc(sizeof(liste_t));
      ptl = ptl->suiv;
      ptl->state = q;
      ptl->suiv = NULL;
      return;
    }

  // we found q in the list, we don't add q
  if(ptl->suiv->state == q)
    {
      return;
    }

  // we add it at the right place
  tmp = ptl->suiv;
  ptl->suiv = (liste_t*) malloc(sizeof(liste_t));
  ptl = ptl->suiv;
  ptl->state = q;
  ptl->suiv = tmp;
}

int compteListe(liste_t* li)
{
  if( !li ){ return 0; }
  
  int count = 0;
  liste_t* itr = li;
	  
  while(itr != NULL)
    {
      count++;
      itr = itr->suiv;
    }
  return count;
}

void creerListe(liste_t* liste)
{
  assert(liste);
  
  liste->state = -1;
  liste->suiv = NULL;
}

void detruitListe(liste_t* liste)
{
  assert(liste);
  
  liste_t* itr = liste;
  liste_t* tmp = itr;
  
  while(tmp != NULL)
    {
      tmp = itr->suiv;
      free(itr);
      
      itr = tmp;
    }
}

void afficheListe(liste_t* liste)
{
  assert(liste);
  
  liste_t* itr = liste;

  while(itr != NULL)
    {
      if( itr->state >= 0 )
	{
	  printf("%d ", itr->state);
	}
      
      itr = itr->suiv;
    }
}

int estVideListe(liste_t* liste)
{
  assert(liste);
  return (liste->suiv == NULL);
}
