#include "automate.h"
//#include "file.h"
#include <stdio.h>

void ajouteTransition(automate_t* automate, size_t beg, size_t end, char label)
{
  short int  transition = ((int)(label) - 'a');

  // check if the automate exists
  assert(automate);
  
  // check if state beg exists
  assert(beg >= 0);
  assert(beg < automate->size);
  
  // check if state end exists
  assert(end >= 0);
  assert(end < automate->size);

  // check if the transition exists
  assert(transition >= 0);
  assert(transition < automate->sizealpha);

  liste_t **li = &automate->trans[beg][transition];
  ajouteListe( li , end );
}

void automate_create(automate_t* automate, size_t size, size_t sizealpha)
{
  assert(automate);
  automate->size = size;
  automate->sizealpha = sizealpha;
  
  automate->initial = calloc(automate->size, sizeof(int));

  automate->final = calloc(automate->size, sizeof(int));

  automate->trans = (liste_t***)(calloc(automate->size, sizeof(liste_t**)));
  
  for(int i=0; i<automate->size; i++)
    {
      automate->trans[i] = (liste_t**)(calloc(automate->sizealpha, sizeof(liste_t*)));

      for(int j=0; j<automate->sizealpha; j++)
	{
	  automate->trans[i][j] = NULL;
	}
    }

  automate->status = AUTOMATE_VALID;
}

void automate_destroy(automate_t* automate)
{
  assert(automate);
  
  for(int i=0; i<automate->size; i++)
    {
      for(int j=0; j<automate->sizealpha; j++)
	{
	  liste_t *current_list = automate->trans[i][j];
	  
	  while( current_list != NULL )
	    {
	      liste_t *next = current_list->suiv;
	      free(current_list);
	      current_list = next;
	    }
   
	}
      
      free(automate->trans[i]);
    }

  free(automate->trans);
  free(automate->initial);
  free(automate->final);
  
  automate->status = AUTOMATE_INVALID;
}

void automate_status(automate_t *automate)
{
  printf("Nombre de transitions : %d\n",   compteTransition(automate));
  printf("Deterministe : %d\n", deterministe(automate));
  printf("Complet : %d\n", complet(automate));
  printf("Vide : %d\n", langageVide(automate));
}

void afficheAutomate(automate_t *automate)
{
  printf("Les états initiaux :\n");
  for(int i=0; i<automate->size; i++)
    {
      if(automate->initial[i] == 1)
	{
	  printf("%d ", i);
	}
    }
  printf("\n");

  printf("Les états finaux :\n");
  for(int i=0; i<automate->size; i++)
    {
      if(automate->final[i] == 1)
	{
	  printf("%d ", i);
	}
    }
  printf("\n");
  
  printf("Les transitions\n");
  for(int state=0; state < automate->size; state++)
    {
      printf("----------------------------------\n");
      printf("Depuis l'etat %d : \n", state);
      
      for(int alpha=0; alpha < automate->sizealpha; alpha++)
	{
	  printf("avec la lettre %c : \n", (char)('a' + alpha));

	  liste_t *it = automate->trans[state][alpha];

	  while( it != NULL )
	    {
	      printf("%d ", it->state);
	      it = it->suiv;
	    }
	  
	  printf("\n");
	}
    }
  
}


int compteTransition(automate_t* automate)
{
  assert(automate);

  int count = 0;
  
  for(int state = 0; state < automate->size; state++)
    {
      for(int alpha = 0; alpha < automate->sizealpha; alpha++)
	{
	  count += compteListe(automate->trans[state][alpha]);
	}
    }

  return count;
}

int deterministe(automate_t* automate)
{
  assert(automate);

  for(int state = 0; state < automate->size; state++)
    {
      for(int alpha = 0; alpha < automate->sizealpha; alpha++)
	{
	  int count = compteListe(automate->trans[state][alpha]);
	  if( count > 1 )
	    {
	      return 0;
	    }
	}
    }
  
  return 1;
}

int complet(automate_t* automate)
{
  for(int state = 0; state < automate->size; state++)
    {
      for(int alpha = 0; alpha < automate->sizealpha; alpha++)
	{
	  if( automate->trans[state][alpha] == NULL )
	    {
	      return 0;
	    }
	}
    }
  
  return 1;
}

void supprimeTransition(automate_t* automate, size_t beg, size_t end, char label)
{
  assert(automate);
  size_t transition = (size_t)(label - 'a');

  liste_t* before_remove = automate->trans[beg][transition];

  // the transition does not exists..
  if( before_remove == NULL )
    {
      return;
    }
  
  // first element of the list
  if( before_remove->state == (int)(end) )
    {
      //remove it !
      automate->trans[beg][transition] = before_remove->suiv;
      free(before_remove);
      return;
    }

  // find it, dead or alive !
  while(before_remove->suiv != NULL && before_remove->suiv->state != (int)(end) )
    {
      before_remove = before_remove->suiv;
    }
  
  // it doesn't exists anyway..
  if( before_remove->suiv == NULL ){ return; }

  // remove it !
  liste_t* to_remove = before_remove->suiv;
  before_remove->suiv = before_remove->suiv->suiv;
  free(to_remove);
}


// special_state = initial or final
int is_the_last_of(automate_t* automate, int* special_state, size_t state)
{
  size_t sum = 0;
  
  for(size_t i = 0; i < automate->size; i++)
    {
      sum += special_state[i];
      if( sum > 1 ){ return 0;}
    }

  return special_state[state];
}

void ajouteEtat(automate_t* automate)
{
  assert(automate);

  // move the old transitions in a bigger array
  liste_t*** new_trans = calloc(automate->size + 1, sizeof(liste_t**));
  
  for(size_t state = 0; state < automate->size; state++)
    {
      new_trans[state] = automate->trans[state];
    }

  free(automate->trans);
  
  // create the last state
  new_trans[automate->size] = calloc(automate->sizealpha, sizeof(liste_t*));
  for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
    {
      new_trans[automate->size][alpha] = NULL;
    }

  automate->size++; 
  automate->trans = new_trans;

  // increase intitial and final array

  // initial
  {
    int* new_initial = calloc(automate->size, sizeof(int));
    memcpy(new_initial, automate->initial, (automate->size - 1) * sizeof(int));
    new_initial[automate->size - 1] = 0;
    free(automate->initial);
    automate->initial = new_initial;
  }

  // final
  {
    int* new_final = calloc(automate->size, sizeof(int));
    memcpy(new_final, automate->final, (automate->size - 1) * sizeof(int));
    new_final[automate->size - 1] = 0;
    free(automate->final);
    automate->final = new_final;
  }
}

void supprimeEtat(automate_t* automate, size_t state_to_remove)
{
  assert(automate);
  assert(state_to_remove < automate->size);
  
  // check if we can delete the state
  if( is_the_last_of(automate, automate->initial, state_to_remove)
      || is_the_last_of(automate, automate->final, state_to_remove) )
    {
      return;
    }

  // delete the state from initial and final states
  automate->initial[state_to_remove] = 0;
  automate->final[state_to_remove] = 0;
     
  // check all the transitions to state_to_remove
  for(size_t state = 0; state < automate->size; state++)
    {
      for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
	{
	  supprimeTransition(automate, state, state_to_remove, (char)(alpha + 'a'));
	  supprimeTransition(automate, state_to_remove, state, (char)(alpha + 'a'));
	}
    }

  //remove the state
  for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
    {
      liste_t* itr = automate->trans[state_to_remove][alpha];

      while( itr != NULL )
	{
	  liste_t* suiv = itr->suiv;
	  free(itr);
	  itr = suiv;
	}
      
      automate->trans[state_to_remove][alpha] = NULL;
    }

  free(automate->trans[state_to_remove]);
  
  // shift the state numbers higher than the deleted one
  for(size_t state = 0; state < automate->size-1; state++)
    {
      if(state >= state_to_remove)
	{
	  automate->trans[state] = automate->trans[state+1];
	  automate->initial[state] = automate->initial[state+1];
      	  automate->final[state] = automate->final[state+1];
	}
      
      // update the transitions according to the previous shiffting
      for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
      	{
	  liste_t* itr = automate->trans[state][alpha];

	  while(itr != NULL)
	    {
	      if( itr->state >= state_to_remove )
		{
		  itr->state--;
		}

	      itr = itr->suiv;
	    }
      	}

    }

	  
  automate->trans[automate->size - 1] = NULL;

  automate->size--;
}

void completeAutomate(automate_t* automate)
{
  assert(automate);
  int puit_state = -1;

  //   ^
  //  / \    automate->size may be increased by ajouteEtat
  // / ! \   but it is not a issue since the puit_state need
  // -----   to be inlude in the completion process
  
  for(size_t state = 0; state < automate->size; state++)
    {
      for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
	{
	  if( automate->trans[state][alpha] == NULL )
	    {
	      if( puit_state == -1 )
		{
		  ajouteEtat(automate);
		  puit_state = automate->size - 1;
		}
	      
	      ajouteTransition(automate, state, puit_state, (char)(alpha + 'a'));
	    }
	}
    }
}

// merge state_1 with state_2 and delete state_2
void fusionEtat(automate_t* automate, size_t state_1, size_t state_2)
{
  assert(automate);
  assert(state_1 < automate->size);
  assert(state_2 < automate->size);

  // merge same state is stupid
  if( state_1 == state_2 ){ return; }

  // transition FROM state_2
  for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
    {
      liste_t* itr = automate->trans[state_2][alpha];
	
      while(itr != NULL)
	{
	  // all the transition from state_2 need to start from state_1
	  ajouteTransition(automate, state_1, itr->state, (char)(alpha + 'a'));
	  itr = itr->suiv;
	}
    }

  // transition TO state_2
  for(size_t state = 0; state < automate->size; state++)
    {
      for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
	{
	  liste_t* itr = automate->trans[state][alpha];
	
	  while(itr != NULL)
	    {
	      // all the transition to state_2 need to end on state_1
	      if( itr->state == state_2 )
		{
		  ajouteTransition(automate, state, state_1, (char)(alpha + 'a'));
		}
	      
	      itr = itr->suiv;
	    }
    
	}
    }

  if( automate->initial[state_2] )
    {
      automate->initial[state_1] = 1;
    }

  if( automate->final[state_2] )
    {
      automate->final[state_1] = 1;
    }

  supprimeEtat(automate, state_2);  
}

void construitAutomateExemple(automate_t* automate)
{
  automate_create(automate, 5, 2);
  
  automate->initial[0] = 1;
  automate->initial[1] = 1;
  automate->final[1] = 1;
  automate->final[4] = 1;
  
  ajouteTransition(automate, 0, 1, 'a');
  ajouteTransition(automate, 0, 2, 'a');
  ajouteTransition(automate, 0, 3, 'a');
  
  ajouteTransition(automate, 1, 3, 'b');
  
  ajouteTransition(automate, 2, 3, 'a');
  ajouteTransition(automate, 2, 4, 'b');
  
  ajouteTransition(automate, 3, 3, 'b');
  ajouteTransition(automate, 3, 4, 'b');
  
  ajouteTransition(automate, 4, 4, 'a');
  
}

void automateToGraph(automate_t* automate, graph_t* graph)
{
  graph_fit_to_size(graph, automate->size);

  // copy the automate
  for(size_t state=0; state<automate->size; state++)
    {
      for(size_t alpha=0; alpha<automate->sizealpha; alpha++)
	{
	  liste_t* itr = automate->trans[state][alpha];

	  while(itr != NULL)
	    {
	      graph_add(graph, state, itr->state);
	      itr = itr->suiv;
	    }
	}
    }
}

int langageVide(automate_t* automate)
{
  assert(automate);
  
  graph_t graph;
  graph_create(&graph);
  automateToGraph(automate, &graph);

  for(size_t initial=0; initial<automate->size; initial++)
    {
      for(size_t final=0; final<automate->size; final++)
	{
	  if(automate->initial[initial] == 1
	     && automate->final[final] == 1)
	    {
	      if( graph_path(&graph, initial, final) == 1 )
		{
		  graph_destroy(&graph);
		  return 0;
		}
	    }
	}
    }
  
  graph_destroy(&graph);
  return 1;
}

void supprime_non_accessible(automate_t *automate)
{
  assert(automate);

  size_t const size = automate->size;
  size_t state = size;

  int* pas_accessible = calloc(size, sizeof(int));

  while(state != 0)
    {
      state--;
      if( !est_accessible(automate, state) )
	{
	  pas_accessible[state] = 1;
	  automate->final[state] = 0;
	}
    }
  
  for(int i=size-1; i>=0; i--)
    {
      if(pas_accessible[i])
      	{
      	  supprimeEtat(automate, i);
	  printf("delete %d\n", i);
      	}
    }
  
  free(pas_accessible);
}

void supprime_non_co_accessible(automate_t *automate)
{
  assert(automate);

  size_t const size = automate->size;
  size_t state = size;

  int* pas_co_accessible = calloc(size, sizeof(int));

  while(state != 0)
    {
      state--;

      if( !est_co_accessible(automate, state) )
	{
	  pas_co_accessible[state] = 1;
	  automate->initial[state] = 0;
	}
    }
  
  for(int i=size-1; i>=0; i--)
    {
      if(pas_co_accessible[i])
      	{
      	  supprimeEtat(automate, i);
      	}
    }
  
  free(pas_co_accessible);
}

int est_accessible(automate_t *automate, size_t state)
{
  assert(automate);
  graph_t graph;
  graph_create(&graph);  
  automateToGraph(automate, &graph);

  if( automate->initial[state] ){ return 1; }
  
  for(size_t initial=0; initial<automate->size; initial++)
    {
      if( automate->initial[initial] == 0 ){ continue; }
      int path_exists = graph_path(&graph, initial, state);

      if( path_exists == 1 )
	{
	  graph_destroy(&graph);
	  return 1;
	}
    }
  
  graph_destroy(&graph);

  return 0;
}


int est_co_accessible(automate_t *automate, size_t state)
{
  assert(automate);
  graph_t graph;
  graph_create(&graph);  
  automateToGraph(automate, &graph);

  if( automate->final[state] ){return 1;}
  
  for(size_t final=0; final<automate->size; final++)
    {
      if( automate->final[final] == 0 ){ continue; }
      int path_exists = graph_path(&graph, state, final);

      if( path_exists == 1 )
	{
	  graph_destroy(&graph);
	  return 1;
	}
    }
  
  graph_destroy(&graph);

  return 0;
}

void automate_graph_print(automate_t* self)
{
  assert(self);

  graph_t graph;
  graph_create(&graph);

  automateToGraph(self, &graph);
  graph_dump(&graph);
  graph_destroy(&graph);

  printf("\n");
}

void automate_create_produit(automate_t* self, automate_t* src1, automate_t* src2)
{
  assert(self);
  assert(src1);
  assert(src2);

  automate_create( self
		   , src1->size * src2->size
		   , (src1->sizealpha > src2->sizealpha)
		   ? src1->sizealpha : src2->sizealpha );

  size_t state_of_src1 = 0;
  size_t state_of_src2 = 0;

  /*
    Iterate on src1 and src2 with state_of_src1 and state_of_src2 
    at the same time. Create the common transitions in both automatas in
    self.
  */
  
  for(size_t state = 0; state < self->size; state++)
    {
      // get accessible states from state_of_src1
      char accessible_state_from_1[src1->size];
      for(size_t i=0; i<src1->size; i++){ accessible_state_from_1[i] = 0; }
      
      for(size_t alpha = 0; alpha < src1->sizealpha; alpha++)
      	{
      	  liste_t *itr = src1->trans[state_of_src1][alpha];

      	  while(itr != NULL)
      	    {
      	      accessible_state_from_1[itr->state] = (char)(alpha + 'a');
      	      itr = itr->suiv;
      	    }
      	}

      // get accessible states from state_of_src2
      char accessible_state_from_2[src2->size];
      for(size_t i=0; i<src2->size; i++){ accessible_state_from_2[i] = 0; }
      
      for(size_t alpha = 0; alpha < src2->sizealpha; alpha++)
      	{
      	  liste_t *itr = src2->trans[state_of_src2][alpha];

      	  while(itr != NULL)
      	    {
      	      accessible_state_from_2[itr->state] = (char)(alpha + 'a');
      	      itr = itr->suiv;
      	    }
      	}

      // add transitions
      for(size_t state1=0; state1 < src1->size; state1++)
	{
	  for(size_t state2=0; state2 < src2->size; state2++)
	    {
	      if( accessible_state_from_1[state1]
		  == accessible_state_from_2[state2]
		  && accessible_state_from_1[state1] != '\0' )
		{
		  // transition from state to accessible_state
		  // by the letter alpha
		  char alpha = (char)(accessible_state_from_1[state1]);
		  size_t accessible_state = state1 * src2->size + state2;

		  ajouteTransition(self, state, accessible_state, alpha);
		}
	    }
	}

      // update state_of_src 1 and 2
      // check if they're initial or final
      if(src1->initial[state_of_src1] == 1
	 && src2->initial[state_of_src2] == 1)
	{
	  self->initial[state] = 1;
	}
      
      if(src1->final[state_of_src1] == 1
	 && src2->final[state_of_src2] == 1)
	{
	  self->final[state] = 1;
	}

      state_of_src2++;
      if( state_of_src2 >= src2->size )
	{
	  state_of_src2 = 0;
	  state_of_src1++;
	}
    }  
}

size_t automate_transitions_from(automate_t* automate, size_t from_state,
				 char from_alpha, int* array)
{
  assert(array);
  assert(from_state < automate->size);
  size_t alpha = from_alpha - 'a';
  assert(alpha < automate->sizealpha);
  
  size_t size = 0;
  
  for(liste_t* it = automate->trans[from_state][alpha];
      it != NULL; it = it->suiv)
    {
      array[it->state] = 1;
      size++;
    }

  return size;
}

void automate_create_deterministe(automate_t* automate, automate_t* model)
{
  assert(automate);
  assert(model);

  automate_create(automate, 0, model->sizealpha);
  
  file_t file;
  file_create(&file);
  
  automate_create_deterministe_init(automate, model, &file);

  // add transitions
  {
    file_node_t* itr = file.first;
    
    while(itr != NULL)
      {
	// via all the letters
	for(size_t alpha=0; alpha<model->sizealpha; alpha++)
	  {
	    
	    automate_create_deterministe_transition_from_letter(automate
								, model
								, &file
								, itr
								, alpha + 'a');
	  }
	    
	itr = itr->next;
      }
  }
  
  file_destroy(&file);
}

void automate_create_deterministe_init(automate_t* automate, automate_t* model, file_t* file)
{
  assert(automate);
  assert(model);
  assert(file);
  
  int tmp[256];
  size_t sz=0;
    
  for(size_t initial=0; initial<model->size; initial++)
    {
      if( !model->initial[initial] ){ continue; }
      tmp[sz] = initial;
      sz++;
    }

  file_add(file, tmp, sz);
  ajouteEtat(automate);
  automate->initial[automate->size - 1] = 1;
}

void automate_create_deterministe_transition_from_letter(automate_t* automate, automate_t* model, file_t* file, file_node_t* current, char letter)
{
  assert(automate);
  assert(model);
  assert(file);
  assert(current);
  
  int reached[model->size];
  memset(reached, 0, sizeof(int)*model->size);
  
  // get reached states
  // from all the states
  for(size_t state=0; state<current->array_size; state++)
    {
      automate_transitions_from(model
  				, current->array[state]
  				, letter
  				, reached);
    }
  
  // build new file_node with reached states array
  // and add it to file
  {
    int tmp[256];
    size_t sz = 0;

    int has_final = 0;
    
    // build it
    for(size_t i=0; i<model->size; i++)
      {
  	if( !reached[i] ){ continue; }
	if( model->final[i] ){ has_final = 1; }
  	tmp[sz] = i;
  	sz++;
      }

    // nothing reached
    if(sz == 0)
      {
	return;
      }

    file_node_t* destination = file_find(file, tmp, sz);
    
    // the destination doesn't exists
    // we have to add it to the file
    if( destination == NULL )
      {
	ajouteEtat(automate);
        automate->final[automate->size - 1] = has_final;
  	file_add(file, tmp, sz);
	destination = file->last;
      }

    ajouteTransition(automate, current->index, destination->index, letter);

  }
}

void automate_complement(automate_t* automate)
{
  assert(automate);

  for(size_t i=0; i<automate->size; i++)
    {
      automate->final[i] ^= 1;
    }
}

int automate_include(automate_t* automate, automate_t* other)
{
  assert(automate);
  assert(other);
  
  automate_complement(other);

  automate_t product;
  automate_create_produit(&product, automate, other);
  int include = langageVide(&product);
  automate_destroy(&product);

  automate_complement(other);

  return include;
}

int automate_is_minimal(automate_t* automate)
{
  assert(automate);

  for(size_t state_1 = 0; state_1 < automate->size; state_1++)
    {
      for(size_t state_2 = 0; state_2 < automate->size; state_2++)
	{
	  // non state is equivalent to an other
	  if(state_1 != state_2)
	    {
	      if( automate_nerode_equivalent(automate, state_1, state_2) )
		{
		  return 0;
		}
	    }
	}
    }
  
  return 1;
}

void automate_build_from_state(automate_t* automate, automate_t* other, size_t from_state)
{
  assert(automate);
  assert(automate->size == 0);
  assert(other);
  assert(from_state < other->size);

  // copy all the accessible state from from_state
  graph_t graph;
  graph_create(&graph);
  automateToGraph(other, &graph);

  size_t other_to_automate[other->size];
  size_t automate_to_other[other->size];
  memset(other_to_automate, -1, sizeof(other_to_automate));
  memset(automate_to_other, -1, sizeof(automate_to_other));
    
  for(size_t state = 0; state < other->size; state++)
    {
      if( graph_path(&graph, from_state, state) )
	{
	  ajouteEtat(automate);
	  automate_to_other[automate->size - 1] = state;
	  other_to_automate[state] = automate->size - 1;

	  automate->initial[automate->size - 1] = other->initial[state];
	  automate->final[automate->size - 1] = other->final[state];
	}
    }

  automate->initial[other_to_automate[from_state]] = 1;
  
  graph_destroy(&graph);

  // copy all the transitions
  for(size_t state = 0; state < automate->size; state++)
    {
      for(size_t alpha = 0; alpha < automate->sizealpha; alpha++)
  	{
  	  for(liste_t* itr = other->trans[automate_to_other[state]][alpha]; itr != NULL; itr = itr->suiv)
  	    {
	      ajouteTransition(automate, state, other_to_automate[itr->state], (char)(alpha + 'a'));
  	    }
  	}
    }
}

int automate_nerode_equivalent(automate_t* automate, size_t s1, size_t s2)
{
  assert(automate);
  assert(s1 < automate->size);
  assert(s2 < automate->size);

  automate_t a1;
  automate_create(&a1, 0, automate->sizealpha);
  automate_build_from_state(&a1, automate, s1);
  
  automate_t a2;
  automate_create(&a2, 0, automate->sizealpha);
  automate_build_from_state(&a2, automate, s2);

  int equivalent = automate->final[s1] == automate->final[s2]
    && automate_include(&a1, &a2)
    && automate_include(&a2, &a1);

  automate_destroy(&a1);
  automate_destroy(&a2);
  
  return equivalent;
}

void automate_minimise_nerode(automate_t* automate)
{
  assert(automate);
  if( automate_is_minimal(automate) ){ return; }

  for(size_t state_1 = 0; state_1 < automate->size; state_1++)
    {
      for(size_t state_2 = 0; state_2 < automate->size; state_2++)
	{
	  // we look at each couple of state only once
	  if(state_1 < state_2)
	    {
	      if( automate_nerode_equivalent(automate, state_1, state_2) )
		{
		  // we fusion the state
		  fusionEtat(automate, state_1, state_2);
		  // and we run the algorithm again
		  // in order not to invalidate the structure
		  // while iterate on it
		  automate_minimise_nerode(automate);
		  return;
		}
	    }
	}
    }
}

void automate_minimise_hopcroft(automate_t* automate)
{
  assert(automate);
  if( automate_is_minimal(automate) ){ return; }

  file_t file;
  file_create(&file);

  // create two classes
  // - not final states
  // - final states
  {
    int others[automate->size];
    int final[automate->size];
    size_t n_others = 0;
    size_t n_final = 0;
  
    for(size_t state = 0; state < automate->size; state++)
      {
	if(automate->final[state])
	  {
	    final[n_final] = state;
	    n_final++;
	  }
	else
	  {
	    others[n_others] = state;
	    n_others++;
	  }
      }

    file_add(&file, others, n_others);
    file_add(&file, final, n_final);
  }

  // create new classes for states
  // by splitting iterativly the existing classes
  // until no splitting are needed anymore
  {
    file_node_t* itr = file.first;

    while(itr != NULL)
      {
	int uniques[itr->array_size];
	int n_uniques = 0;

	// looking for unique states
	for(size_t state_1=0; state_1<itr->array_size; state_1++)
	  {
	    int is_unique = 1;
	    for(size_t state_2=0; state_2<itr->array_size; state_2++)
	      {
		if(state_1 == state_2){ continue; }
		
		int equivalent = automate_nerode_equivalent(automate
							    , itr->array[state_1]
							    , itr->array[state_2]);
		
		if(equivalent)
		  {
		    is_unique = 0;
		    break;
		  }
	      }

	    // add it to the unique states array
	    if(is_unique)
	      {
		int unique_state = itr->array[state_1];
		uniques[n_uniques] = unique_state;
		n_uniques++;
	      }
	  }

	// create a new class if needed
	if( !file_contains(&file, uniques, n_uniques)
	   && n_uniques > 0)
	  {
	    for(size_t state=0; state<n_uniques; state++)
	      {
		// delete the unique state from the file
		int unique_state = uniques[state];
		itr->array[uniques[state]] = itr->array[itr->array_size - 1];
		itr->array[itr->array_size - 1] = unique_state;
		itr->array_size--;
	      }

	    // add the new class
	    file_add(&file, uniques, n_uniques);
	  }
	
	itr = itr->next;
      }
  }

  // apply the modification on the automata
  automate_minimise_hopcroft_fusion(automate, &file);

  file_destroy(&file);
}

void automate_minimise_hopcroft_fusion(automate_t* automate, file_t* file)
{
  assert(automate);
  assert(file);
  
  file_node_t* itr = file->first;
  
  while(itr != NULL)
    {
      size_t first_state = itr->array[0];
      
      while(itr->array_size > 1)
	{
	  fusionEtat(automate, first_state, itr->array[1]);

	  // Update file structure
	  // delete the state_to_fusion
	  int state_to_fusion = itr->array[1];
	  itr->array[1] = itr->array[itr->array_size - 1];
	  itr->array[itr->array_size - 1] = state_to_fusion;
	  itr->array_size--;

	  // update all the others state in the file
	  // according to the fusion
	  for(file_node_t* update_itr = itr; update_itr != NULL; update_itr = update_itr->next)
	    {
	      for(size_t update = 0; update < update_itr->array_size; update++)
		{
		  if(update_itr->array[update] >= state_to_fusion)
		    {
		      update_itr->array[update]--;
		    }
		}
	    }
	}
      
      itr = itr->next;
    }

}
