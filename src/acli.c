#include <acli.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void print_title()
{
  printf("\n\e[32m~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("~ OSSETE GOMBE Berenger : Theorie des Langages, TP1 ~\n");
  printf("~~~~~~~~~~~~~~~~~~~~~ Octobre 2015 ~~~~~~~~~~~~~~~~~~\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\e[0m\n\n");
}


void print_menu(acli_t* acli)
{
  assert(acli);
  
  printf("\n\e[36m");
  printf("-------------------------\n");
  printf("s) Switch automata\n");
  printf("bn) Build a new automata\n");
  printf("dc) Delete current automata\n");
  printf("be) Build an example of automata\n");
  printf("-------------------------\n");
  printf("i)  Define state as initial\n");
  printf("f)  Define state as final\n");
  printf("-------------------------\n");
  printf("di) Display the automata\n");
  printf("p)  Print info\n");
  printf("-------------------------\n");
  printf("at) Add a transition\n");
  printf("dt) Delete a transition\n");
  printf("-------------------------\n");
  printf("dna) Delete none accessible states\n");
  printf("dnca) Delete none co-accessible states\n");
  printf("-------------------------\n");
  printf("as) Add a state\n");
  printf("fs) Fusion two state\n");
  printf("ds) Delete a state\n");
  printf("-------------------------\n");
  printf("c)  Complete the automata\n");
  printf("mn)  Minimise the automata using nerode algorithm\n");
  printf("mh)  Minimise the automata using hopcroft algorithm\n");
  printf("cc) Compute complement\n");
  printf("-------------------------\n");
  printf("pr) Product of two automatas\n");
  printf("cd) Create deterministe automata\n");
  printf("-------------------------\n");
  printf("q)  Quit\n");
  printf("\e[0m\n\n");

  printf("Current automata : %zd\n\n\n", acli->current_automate);
}

void get_user_char(char* str, size_t size)
{
  size_t sz = 0;
  
  while( sz < size && read(0, str+sz, sizeof(char) ) != 0 )
    {
      if( str[sz] == '\n' )
	{
	  str[sz] = '\0';
	  return;
	}
      
      sz++;
    }
  
  str[size-1] = '\0';
 
  char buf;
  while( read(0, &buf, sizeof(char)) != 0 && buf != '\n'){}
}

int execute_function(acli_t* acli, char* name)
{
  assert(acli);
  assert(name);
  
  automate_t* automate = acli->automates[acli->current_automate];

  if( strcmp(name, "s") == 0 ){ switch_automate(acli); return 1; }
  if( strcmp(name, "q") == 0 ){ return 1; }
  if( strcmp(name, "bn") == 0 ){ build_new(automate); return 1; }
  if( strcmp(name, "be") == 0 ){ build_example(acli); return 1; }
  if( strcmp(name, "pr") == 0 ){ compute_product(acli); return 1; }
  if( strcmp(name, "cd") == 0 ){ compute_deterministe(acli); return 1; }
  
  if( automate->status != AUTOMATE_VALID )
    {
      printf("You have to create the automata before doing that !\n");
      return 0;
    }

  if( strcmp(name, "i") == 0 ){ define_initial(automate); return 1; }
  if( strcmp(name, "f") == 0 ){ define_final(automate); return 1; }     
  if( strcmp(name, "di") == 0 )
    {
      afficheAutomate(automate);
      automate_graph_print(automate);
      return 1;
    }     
  if( strcmp(name, "at") == 0 ){ add_transition(automate); return 1; }
  if( strcmp(name, "dt") == 0 ){ delete_transition(automate); return 1; }
  if( strcmp(name, "dna") == 0 ){ delete_none_accessible(automate); return 1; }
  if( strcmp(name, "dnca") == 0 ){ delete_none_co_accessible(automate); return 1; }
  if( strcmp(name, "as") == 0 ){ add_state(automate); return 1; }
  if( strcmp(name, "fs") == 0 ){ fusion_state(automate); return 1; }
  if( strcmp(name, "ds") == 0 ){ delete_state(automate); return 1; }
  if( strcmp(name, "c") == 0 ){ complete(automate); return 1; }
  if( strcmp(name, "mn") == 0 ){ compute_minimise_nerode(acli); return 1; }
  if( strcmp(name, "mh") == 0 ){ compute_minimise_hopcroft(acli); return 1; }
  if( strcmp(name, "cc") == 0 ){ compute_complement(acli); return 1; }
  if( strcmp(name, "p") == 0 ){ print_info(automate); return 1; }
  if( strcmp(name, "dc") == 0 ){ delete_current(acli); return 1; }
  
  printf("I don't understand this command.\n");
  
  return 0;
}

void build_example(acli_t* acli)
{
  assert(acli);
  
  automate_t* automate = acli->automates[acli->current_automate];
  
  if( automate->status == AUTOMATE_VALID )
    {
      automate_destroy(automate);
    }

  printf("Please, select which example to generate\n");
  printf("\t1) First example of the exercice\n");
  printf("\t2) Product example 1\n");
  printf("\t3) Product example 2\n");
  printf("\t4) TP du 16/12/15 : A1\n");
  printf("\t5) TP du 16/12/15 : A2\n");
  printf("\t6) Determinisation example\n");
  printf("\t7) Minimisation example from wikipedia\n");
  printf("Your choice: ");
  int num;
  scanf("%d", &num);
  
  switch(num)
    {
    case 1:
      construitAutomateExemple(automate);
      break;
    case 2 :
      automate_create(automate, 2, 2);

      automate->initial[0] = 1;
      automate->final[1] = 1;
      ajouteTransition(automate, 0, 0, 'a');
      ajouteTransition(automate, 0, 1, 'b');
      ajouteTransition(automate, 1, 1, 'a');
      break;
    case 3:
      automate_create(automate, 3, 2);

      automate->initial[0] = 1;
      automate->final[2] = 1;
      ajouteTransition(automate, 0, 0, 'b');
      ajouteTransition(automate, 1, 1, 'b');
      ajouteTransition(automate, 2, 2, 'b');
      ajouteTransition(automate, 0, 1, 'a');
      ajouteTransition(automate, 1, 2, 'b');
      break;
    case 4:// A1
      automate_create(automate, 5, 2);

      automate->initial[0] = 1;
      automate->final[1] = 1;
      automate->final[4] = 1;
      
      ajouteTransition(automate, 0, 1, 'a');
      ajouteTransition(automate, 0, 3, 'a');
      ajouteTransition(automate, 0, 2, 'a');
      ajouteTransition(automate, 1, 3, 'b');
      ajouteTransition(automate, 2, 3, 'a');
      ajouteTransition(automate, 2, 4, 'b');
      ajouteTransition(automate, 3, 3, 'b');
      ajouteTransition(automate, 3, 4, 'b');
      ajouteTransition(automate, 4, 4, 'a');      
      break;
      
    case 5:// A2
      automate_create(automate, 2, 2);

      automate->initial[0] = 1;
      automate->final[1] = 1;
      
      ajouteTransition(automate, 0, 0, 'a');
      ajouteTransition(automate, 0, 1, 'b');
      break;

    case 6:
      automate_create(automate, 3, 2);
      automate->initial[0] = 1;
      automate->final[2] = 1;
  
      ajouteTransition(automate, 0, 0, 'a');
      ajouteTransition(automate, 0, 0, 'b');
      ajouteTransition(automate, 0, 1, 'a');
      ajouteTransition(automate, 1, 2, 'a');
      ajouteTransition(automate, 1, 2, 'b');
      break;

    case 7:
      automate_create(automate, 6, 2);
      automate->initial[0] = 1;
      automate->final[2] = 1;
      automate->final[3] = 1;
      automate->final[4] = 1;
      ajouteTransition(automate, 0, 1, 'a');
      ajouteTransition(automate, 0, 2, 'b');
      ajouteTransition(automate, 1, 0, 'a');
      ajouteTransition(automate, 1, 3, 'b');
      ajouteTransition(automate, 2, 4, 'a');
      ajouteTransition(automate, 2, 5, 'b');
      ajouteTransition(automate, 3, 4, 'a');
      ajouteTransition(automate, 3, 5, 'b');
      ajouteTransition(automate, 4, 4, 'a');
      ajouteTransition(automate, 4, 5, 'b');
      ajouteTransition(automate, 5, 5, 'a');
      ajouteTransition(automate, 5, 5, 'b');
      break;
      
    default:
      printf("Unknown number given !\n");
      return;
      break;
    }
}

void build_new(automate_t* automate)
{
  if( automate->status == AUTOMATE_VALID )
    {
      automate_destroy(automate);
    }

  printf("Size of the automata ? ");
  size_t sz;
  scanf("%zd", &sz);

  printf("Size of the alpha ? ");
  size_t sza;
  scanf("%zd", &sza);

  automate_create(automate, sz, sza);
}

void get_user_transition(automate_t* automate, size_t* start, size_t* end, char* label)
{
  assert(start);
  assert(end);
  assert(label);
  
  int const tmp_sz = 256;
  char tmp[100];

  char* msg = "Start state ? ";
  
  do
    {
      write(1, msg, sizeof(char)*strlen(msg));
      get_user_char(tmp, tmp_sz);
      *start = atoi(tmp);
    }
  while(*start < 0 || *start >= automate->size);
  
  do
    {
      msg = "End state ? ";
      write(1, msg, sizeof(char)*strlen(msg));
      get_user_char(tmp, tmp_sz);
      *end = atoi(tmp);
    }
  while(*end < 0 || *end >= automate->size);

  do
    {
      msg = "Label ? ";
      write(1, msg, sizeof(char)*strlen(msg));
      get_user_char(tmp, tmp_sz);
      *label = tmp[0];
    }
  while(*label < 'a' || *label > 'a' + automate->sizealpha );
}

int get_user_state(automate_t* automate, size_t* state)
{
  if(automate->size == 0)
    {
      printf("No remaining states.\n");
      return 0;
    }
  
  do
    {
      printf("State ? ");
      scanf("%zd", state);
    }
  while(*state < 0 || *state >= automate->size);

  return 1;
}

void add_transition(automate_t* automate)
{
  size_t start;
  size_t end;
  char label;

  get_user_transition(automate, &start, &end, &label);
  
  ajouteTransition(automate, start, end, label);
}

void delete_transition(automate_t* automate)
{
  size_t start;
  size_t end;
  char label;

  get_user_transition(automate, &start, &end, &label);
  
  supprimeTransition(automate, start, end, label);
}

void add_state(automate_t* automate)
{
  ajouteEtat(automate);
  printf("State %d has been added.\n", automate->size);
}

void delete_state(automate_t* automate)
{
  size_t state;

  if( get_user_state(automate, &state) )
    {
      supprimeEtat(automate, state);
    }
  else
    {
      fprintf(stderr, "E : Can't delete state !\n");
    }
}

void complete(automate_t* automate)
{
  completeAutomate(automate);
  printf("The automata has been completed.\n");
}

void print_info(automate_t* automate)
{
  automate_status(automate);
}

void define_initial(automate_t* automate)
{
  size_t state = 0;
  if( get_user_state(automate, &state) )
    {
      char answer[256];
      printf("%zd has to be initial ? (y/N)\n", state);
      scanf("%s", answer);
      automate->initial[state] = ( strcmp(answer, "y") == 0 );
    }
  else{ fprintf(stderr, "E : Can't define initial value !\n"); }
}

void define_final(automate_t* automate)
{
  size_t state = 0;
  if( get_user_state(automate, &state) )
    {
      char answer[256];
      printf("%zd has to be final ? (y/N)\n", state);
      scanf("%s", answer);
      automate->initial[state] = ( strcmp(answer, "y") == 0 );
    }
  else{ fprintf(stderr, "E : Can't define final value !\n"); }
}

void delete_none_accessible(automate_t* automate)
{
  assert(automate);
  supprime_non_accessible(automate);
  printf("None accessible state has been deleted.\n");
}

void delete_none_co_accessible(automate_t* automate)
{
  assert(automate);
  supprime_non_co_accessible(automate);
  printf("None coaccessible state has been deleted.\n");
}

void switch_automate(acli_t* acli)
{
  assert(acli);

  printf("Which automate do you want to work with ?\n");

  int n;
  scanf("%d", &n);
  
  if( n < 0 || n >= N_MAX_AUTOMATE )
    {
      printf("Invalid automata number !\n");
      return;
    }
  
  acli->current_automate = n;
  
  printf("Switched to automata number %d.\n", n);
}

void acli_init(acli_t* self)
{
  assert(self);
  self->current_automate = 0;

  for(size_t i = 0; i<N_MAX_AUTOMATE; i++)
    {
      self->automates[i] = malloc(sizeof(automate_t));
      self->automates[i]->status = AUTOMATE_INVALID;
    }
}

void acli_destroy(acli_t* self)
{
  assert(self);
  
  for(size_t i=0; i<N_MAX_AUTOMATE; i++)
    {
      if( self->automates[i]->status == AUTOMATE_VALID )
	{
	  automate_destroy(self->automates[i]);
	}
      
      free(self->automates[i]);
    }
}

void fusion_state(automate_t* automate)
{
  assert(automate);
  
  size_t state1;
  size_t state2;

  if( get_user_state(automate, &state1)
      && get_user_state(automate, &state2))
    {
      fusionEtat(automate, state1, state2);
    }
  else
    {
      fprintf(stderr, "E : Can't fusion state !\n");
    }
}

void compute_product(acli_t* acli)
{  
  printf("Automata 1 : ");
  int a1_num;
  scanf("%d", &a1_num);

  printf("\nAutomata 2 : ");
  int a2_num;
  scanf("%d", &a2_num);

  printf("\nResulting automata : ");
  int result_num;
  scanf("%d", &result_num);

  automate_t* a1 = acli->automates[a1_num];
  automate_t* a2 = acli->automates[a2_num];
  automate_t* result = acli->automates[result_num];

  if( a1->status == AUTOMATE_INVALID )
    {
      printf("Please, create the first automata before doing this.\n");
      return;
    }

  if( a2->status == AUTOMATE_INVALID )
    {
      printf("Please, create the second automata before doing this.\n");
      return;
    }
  
  if( result->status == AUTOMATE_VALID )
    {
      printf("The resulting automata is already used for this session.\n");
      return;
    }

  if( a1 == a2 || a1 == result )
    {
      printf("Please, choose three differents automata.\n");
      return;
    }
  
  automate_create_produit(result, a1, a2);
  
  printf("%d is now the product of %d and %d\n", result_num, a1_num, a2_num);
}

void compute_deterministe(acli_t* acli)
{  
  printf("Automata 1 : ");
  int a1_num;
  scanf("%d", &a1_num);

  printf("\nResulting automata : ");
  int result_num;
  scanf("%d", &result_num);

  automate_t* a1 = acli->automates[a1_num];
  automate_t* result = acli->automates[result_num];

  if( a1->status == AUTOMATE_INVALID )
    {
      printf("Please, create the first automata before doing this.\n");
      return;
    }

  if( result->status == AUTOMATE_VALID )
    {
      printf("The resulting automata is already used for this session.\n");
      return;
    }

  if( a1 == result )
    {
      printf("Please, choose two differents automata.\n");
      return;
    }
  
  automate_create_deterministe(result, a1);
  
  printf("%d is now the determinist version of %d\n", result_num, a1_num);
}

void delete_current(acli_t *acli)
{
  assert(acli);
  automate_t* automate = acli->automates[acli->current_automate];

  if( automate->status == AUTOMATE_INVALID )
    {
      printf("No automata to delete here\n");
      return;
    }

  automate_destroy(automate);
  automate->status = AUTOMATE_INVALID;

}

void compute_complement(acli_t* acli)
{
  assert(acli);
  automate_t* automate = acli->automates[acli->current_automate];
  automate_complement(automate);

  printf("Complement done\n");
}

void compute_minimise_nerode(acli_t* acli)
{
  assert(acli);
  automate_t* automate = acli->automates[acli->current_automate];
  automate_minimise_nerode(automate);

  printf("Minimisation done\n");
}

void compute_minimise_hopcroft(acli_t* acli)
{
  assert(acli);
  automate_t* automate = acli->automates[acli->current_automate];
  automate_minimise_hopcroft(automate);

  printf("Minimisation done\n");
}
