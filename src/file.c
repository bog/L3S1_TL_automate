#include <file.h>

void file_create(file_t* self)
{
  assert(self);
  
  self->first = NULL;
  self->last = NULL;
}

void file_destroy(file_t *self)
{
  assert(self);

  file_node_t* iterator = self->first;

  while(iterator != NULL)
    {
      file_node_t* next = iterator->next;
      file_node_destroy(iterator);
      free(iterator);
      iterator = next;
    }
}

void file_add(file_t* self, int* array, size_t array_size)
{
  assert(self);
  
  file_node_t* new_node = malloc(sizeof(file_node_t));
  file_node_init(new_node);

  file_node_set(new_node, array, array_size);

  if( self->first == NULL )
    {
      self->first = new_node;
      self->last = new_node;
      return;
    }

  new_node->index = self->last->index + 1;
  self->last->next = new_node;
  self->last = new_node;
  new_node->next = NULL;
}

file_node_t* file_find(file_t* self, int* array, size_t array_size)
{
  assert(self);
  if(self->first == NULL){ return 0; }
  
  file_node_t* iterator = self->first;
  
  while(iterator != NULL)
    {
      if( file_node_equals(iterator, array, array_size) )
	{
	  return iterator;
	}
      
      iterator = iterator->next;
    }
  
  return NULL;
}

int file_contains(file_t* self, int* array, size_t array_size)
{
  return (file_find(self, array, array_size) != NULL);
}

void file_dump(file_t* self)
{
  assert(self);

  file_node_t* iterator = self->first;

  printf("--------------------------\n");
  
  while(iterator != NULL)
    {
      printf("Element %zd\t", iterator->index);
      file_node_dump(iterator);
      printf("\n");
      iterator = iterator->next;
    }
}

void file_node_init(file_node_t* self)
{
  assert(self);
  self->array = NULL;
  self->array_size = 0;
  self->index = 0;
  self->next = NULL;
}

int file_node_equals(file_node_t* self, int* array, size_t size)
{
  assert(self);

  if(self->array_size != size){ return 0; }

  for(size_t i=0; i<self->array_size; i++)
    {
      if(self->array[i] != array[i])
	{
	  return 0;
	}
    }

  return 1;
}

void file_node_set(file_node_t* self, int* array, size_t size)
{
  assert(self);
  assert(array);

  if( self->array != NULL )
    {
      free(self->array);
    }
  
  self->array_size = size;
  self->array = calloc(self->array_size, sizeof(int));

  for(size_t i=0; i<self->array_size; i++)
    {
      self->array[i] = array[i];
    }
}

void file_node_dump(file_node_t* self)
{
  assert(self);
  if(self->array_size == 0){ return; }

  for(size_t i=0; i<self->array_size; i++)
    {
      printf("%d", self->array[i]);
    }
}

void file_node_destroy(file_node_t* self)
{
  assert(self);
  free(self->array);
}
