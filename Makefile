EXEC=automate.elf
C=gcc
CFLAGS= -g -Wall -Werror -std=c99 -I"include/"
SRC=$(wildcard *.c) $(wildcard src/*.c)
OBJ=$(SRC:.c=.o)

$(EXEC) : $(OBJ)
	$(C) $(CFLAGS) $^ -o $@

clean:
	rm -rf $(OBJ)

mrproper: clean
	rm -rf $(EXEC)
