#include <stdio.h>
#include <stdlib.h>
#include <acli.h>
#include <file.h>

void test_accessible();
void test_interface();
void test_determinisation();
void test_minimisation();
void test_supprime();
void test_file();
void exit_function();

acli_t acli_g; /* global acli instance */

int main(int argc, char** argv)
{
  test_interface();
  //test_minimisation();
  return 0;
}

void test_minimisation()
{
  automate_t automate;
  automate_create(&automate, 6, 2);
  automate.initial[0] = 1;
  automate.final[2] = 1;
  automate.final[3] = 1;
  automate.final[4] = 1;
  
  ajouteTransition(&automate, 0, 1, 'a');
  ajouteTransition(&automate, 0, 2, 'b');

  ajouteTransition(&automate, 1, 0, 'a');
  ajouteTransition(&automate, 1, 3, 'b');

  ajouteTransition(&automate, 2, 4, 'a');
  ajouteTransition(&automate, 2, 5, 'b');

  ajouteTransition(&automate, 3, 4, 'a');
  ajouteTransition(&automate, 3, 5, 'b');

  ajouteTransition(&automate, 4, 4, 'a');
  ajouteTransition(&automate, 4, 5, 'b');

  ajouteTransition(&automate, 5, 5, 'a');
  ajouteTransition(&automate, 5, 5, 'b');

  // a 0
  // b 1
  // c 2
  // d 3
  
  /* automate_create(&automate, 4, 2); */
  /* automate.initial[0] = 1; */
  /* automate.final[2] = 1; */
  /* automate.final[3] = 1; */
  
  /* ajouteTransition(&automate, 0, 1, 'a'); */
  /* ajouteTransition(&automate, 0, 2, 'b'); */

  /* ajouteTransition(&automate, 1, 3, 'a'); */
  /* ajouteTransition(&automate, 1, 3, 'b'); */

  /* ajouteTransition(&automate, 2, 2, 'a'); */
  /* ajouteTransition(&automate, 2, 2, 'b'); */

  /* ajouteTransition(&automate, 3, 3, 'a'); */
  /* ajouteTransition(&automate, 3, 3, 'b'); */
  //automate_graph_print(&automate);
  
  automate_minimise_hopcroft(&automate);
  //fusionEtat(&automate, 0, 1);
  
  afficheAutomate(&automate);

  /*
    0| 0 1 
    1| 1 2 
    2| 2 
  */
  //automate_graph_print(&automate);
  
  automate_destroy(&automate);

}

void test_determinisation()
{
  automate_t automate;
  /* automate_create(&automate, 3, 2); */
  /* automate.initial[0] = 1; */
  /* automate.final[2] = 1; */
  
  /* ajouteTransition(&automate, 0, 0, 'a'); */
  /* ajouteTransition(&automate, 0, 0, 'b'); */
  /* ajouteTransition(&automate, 0, 1, 'a'); */
  /* ajouteTransition(&automate, 1, 2, 'a'); */
  /* ajouteTransition(&automate, 1, 2, 'b'); */
  
  /* { */
  /*   automate_create(&automate, 5, 2); */

  /*   automate.initial[0] = 1; */
  /*   automate.initial[1] = 1; */
  /*   automate.final[1] = 1; */
  /*   automate.final[4] = 1; */
      
  /*   ajouteTransition(&automate, 0, 1, 'a'); */
  /*   ajouteTransition(&automate, 0, 3, 'a'); */
  /*   ajouteTransition(&automate, 0, 2, 'a'); */
  /*   ajouteTransition(&automate, 1, 3, 'b'); */
  /*   ajouteTransition(&automate, 2, 3, 'a'); */
  /*   ajouteTransition(&automate, 2, 4, 'b'); */
  /*   ajouteTransition(&automate, 3, 3, 'b'); */
  /*   ajouteTransition(&automate, 3, 4, 'b'); */
  /*   ajouteTransition(&automate, 4, 4, 'a'); */
  /* }    */

  {
    automate_create(&automate, 6, 2);
    automate.initial[0] = 1;
    automate.initial[3] = 1;
    automate.final[5] = 1;

    ajouteTransition(&automate, 0, 1, 'a');
    ajouteTransition(&automate, 0, 3, 'a');
    ajouteTransition(&automate, 0, 4, 'b');
    
    ajouteTransition(&automate, 1, 1, 'a');
    ajouteTransition(&automate, 1, 2, 'b');
    
    ajouteTransition(&automate, 2, 2, 'a');
    ajouteTransition(&automate, 2, 2, 'b');
    ajouteTransition(&automate, 2, 1, 'a');
    ajouteTransition(&automate, 2, 5, 'b');
    
    ajouteTransition(&automate, 3, 4, 'b');
    
    ajouteTransition(&automate, 4, 5, 'a');
  }
  
  automate_t deterministe;
  automate_create_deterministe(&deterministe, &automate);

  afficheAutomate(&deterministe);
  printf("\n");
  automate_graph_print(&deterministe);
  automate_destroy(&deterministe);
  automate_destroy(&automate);
}

void test_supprime()
{
  automate_t automate;
  automate_create(&automate, 5, 2);
  automate.initial[0] = 1;
  automate.final[3] = 1;
  automate.final[4] = 1;
  
  ajouteTransition(&automate, 0, 1, 'a');
  ajouteTransition(&automate, 0, 2, 'a');
  ajouteTransition(&automate, 0, 3, 'a');
  ajouteTransition(&automate, 3, 1, 'a');
  ajouteTransition(&automate, 4, 1, 'a');

  supprimeEtat(&automate, 3);
  
  afficheAutomate(&automate);
  automate_graph_print(&automate);
  
  automate_destroy(&automate);
}

void test_file()
{
  file_t f;
  file_create(&f);

  int a[5] = {5, 4, 3, 2, 1};
  file_add(&f, a, 5);
  
  int b[2] = {6, 5};
  file_add(&f, b, 2);
  
  file_dump(&f);

  
  file_destroy(&f);
}

void test_interface()
{
  acli_init(&acli_g);
  
  int quit = 0;

  print_title();
  
  while( quit == 0 )
    {
      print_menu(&acli_g);
      
      int const USER_RESPONSE_MAX = 16;
      char str[USER_RESPONSE_MAX];
      get_user_char(str, USER_RESPONSE_MAX);

      if( strcmp(str, "q") == 0 ){ quit = 1; }
      
      if( !execute_function(&acli_g, str) )
	{
	  fprintf(stderr, "\e[31mAn error occured !\n\e[0m");
	  getchar();
	}
      
      printf("\n");
      
    }
  
  atexit(&exit_function);
}

void test_accessible()
{
  /* a */
  automate_t a;
  automate_create(&a, 2, 2);

  a.initial[0] = 1;
  a.final[1] = 1;
  ajouteTransition(&a, 0, 0, 'a');
  ajouteTransition(&a, 0, 1, 'b');
  ajouteTransition(&a, 1, 1, 'a');
  
  /* b */
  automate_t b;
  automate_create(&b, 3, 2);

  b.initial[0] = 1;
  b.final[2] = 1;
  ajouteTransition(&b, 0, 0, 'b');
  ajouteTransition(&b, 1, 1, 'b');
  ajouteTransition(&b, 2, 2, 'b');
  ajouteTransition(&b, 0, 1, 'a');
  ajouteTransition(&b, 1, 2, 'b');
  
  /* p */
  automate_t p;
  automate_create_produit(&p, &a, &b);
  //supprime_non_accessible(&p);
  //supprime_non_co_accessible(&p);
  //afficheAutomate(&p);
  
  automate_graph_print(&a);
  automate_graph_print(&b);
  automate_graph_print(&p);

  //afficheAutomate(&p);
  automate_destroy(&a);
  automate_destroy(&b);
  automate_destroy(&p);
}

void exit_function()
{
  acli_destroy(&acli_g);
  printf("Good Bye ! \e[32m;-)\e[0m\n");
}
